// Service
//
// esFactory() creates a configured client instance. Turn that instance
// into a service so that it can be required by other parts of the application
angular.module("vic").service('client', function (esFactory) {

  // var invocation = new XMLHttpRequest();
  // var url = 'http://tbbc-staging.cloudapp.net:9200';
     
  // function callOtherDomain() {
  //   if(invocation) {    
  //     invocation.open('GET', url, true);
  //     invocation.onreadystatechange = handler;
  //     invocation.send(); 
  //   }
  // }

  return esFactory({
     host: 'tbbc-staging.cloudapp.net:9200',
    //host: '168.62.201.14:9200',
    //host: 'localhost:9200',
    apiVersion: '1.2',
    log: 'trace'
  });
});