angular.module("vic").controller("AppsListCtrl", ['$scope', 'client', 'esFactory', '$meteor',
  function($scope, client, esFactory, $meteor){ 
    
    $scope.results = [];

    $scope.getAttributes = function(hit){
      //var name = hit.name;   
      var app =  {
        name: hit[0].name, 
        category: hit[0].category,
        price: hit[0].price,
        score: hit[0].score
      }
      console.log(hit[0]);
      return app;
    }


    $scope.search = function(){
      Session.set('error', null);
      Session.set('results', null); 

      var user = $('#user').val();
      var repo = $('#repo').val();
      var keyword = $('#keyword').val();


      $('#search').html('Searching...');  

      client.search({
        index: 'play_store_v1',
        type: 'apps'        
        // q: 'developer_website:https://www.google.com/url?q=http://www.dream-up.eu/game&sa=D&usg=AFQjCNFvSQxfbYdVr8zAGzHMFUZQ9XM31g'
      }).then(function (resp) {
          var hits = resp.hits;        
          $('#search').html('Search Now');
          // Session.set('results', hits);  
          $scope.results = hits.hits;
          console.log(hits.hits);
      }, function (err) {
          console.trace(err.message);
          Session.set('error', err.message);
      }); 
    }
}]);