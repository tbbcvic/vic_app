angular.module("vic").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
	function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('search', {
        url: '/search',
        templateUrl: 'client/vic/views/search.ng.html',
        controller: 'AppsListCtrl'
      })
      .state('appsDetails', {
        url: '/apps/:appId',
        templateUrl: 'client/vic/views/app-details.ng.html',
        controller: 'AppDetailsCtrl'
      });

    $urlRouterProvider.otherwise("/search");
}]);